# SatNOGS Optical Unofficial Guide
The latest version of the SatNOGS Optical Unofficial Guide PDF is available
for download in the Releases section of the source code archive:

* https://spacecruft.org/spacecruft/SNOUG/releases


# Build from Source
To build the PDF documentation from the LaTeX source code,
see the `BUILD.md` document.


# License
SatNOGS Optical Unofficial Guide

by Jeff Moe

Copyright &copy; 2022, Jeff Moe

Permission is granted to copy, distribute and/or modify this document under
the terms of the Creative Commons Attribution 4.0 International Public License
(CC BY-SA 4.0).

Published by Jeff Moe, Loveland, Colorado, USA.

