# SatNOGS Optical Unofficial Guide Build
This `BUILD.md` document is about how to build the documentation,
not about how to build a ground station.

The *SatNOGS Optical Unofficial Guide* is written in LaTeX
which generates a final PDF for viewing or printing.


# Install

* Requires LaTeX. Note, LaTeX is in the `texlive` packages in Debian
  and is relatively huge (1 gig+ install).

* Minted syntax highlighting requires python3-pygments to be installed.

* Requires more packages than just below. XXX

Install dependencies in Debian:

```
sudo apt update
sudo apt install texlive-latex-recommended texlive-xetex \
    texlive-humanities texlive-pstricks \
    python3-pygments \
    fonts-gemunu-libre fonts-linuxlibertine fonts-stix \
    biber git
```

Get source code:

```
git clone https://spacecruft.org/spacecruft/SNOUG
cd SNOUG/
```


# Build
Run the build script to make a PDF:

```
make
```

To just make a cover:

```
make cover
```

To clean:

```
make clean
```


# View
Use a PDF viewer to read the document, such as:

```
evince SNOUG.pdf
```


# Book Parts
The book source has many features in the LaTeX code.
It includes all parts generally used in non-fiction books.
Unused parts can be easily disabled.

## Front Matter
* Half Title.
* Frontispiece.
* Title page.
* Copyright page.
* Dedication.
* Epigraph.
* Table of Contents.
* List of Figures.
* List of Tables.
* Foreward.
* Preface.
* Acknowledgements.
* Introduction.
* Prologue.

## Body Matter
* Chapters.
* Sections.
* Subsections.
* Subsubsections...

## Back Matter
* Appendix.
* Glossary.
* Bibliography.
* Index.
* Colophon.

# Features
Features in the LaTeX code.

* Makefile for "easy" LaTeX processing.
* Auto-generated Table of Contents, List of Figures, Index, etc.
* Syntax highlighting of computer source code.
* BibLaTeX bibliographic system using external `jabref` application for managing
  entries.
* Footnotes.
* Glossary and acronym processing.
* "Crown Quarto/Crown 4vo" size (7.44 x 9.68", 18.90 x 24.58cm) in use at
  large printing houses (e.g. Lulu/Lightning Source).
* Microtyping with OTF fonts.
* Internal hyperlinks from Table of Contents.
* Internal hyperlinks to footnotes.
* Internal hyperlinks to glossary.
* Internel hyperlinks from index.
* External hyperlinks from references and other URLs.
* Generated ISBN bar code on Half Title page.
* Generated date stamp on Copyright page.
* Use of roman in front matter then arabic page numerals for the body.
* Generation of graphics, diagrams, flow charts, etc., within document.
* Custom colors.
* Custom fonts.
* Custom page style.
* Custom chapter style.
* Unicode UTF-8 (entirely?).
* PDF titling.
* Figures.
* Tables.
* Lists.
* Graphics.
* Labels.
* Internal citations.
* External citations.

# BibTeX
If `kbibtex` seg faults, perhaps:

```
rm -f -r .local/share/kbibtex .cache/kbibtex/ .config/kbibtexrc
```

Use `jabref`.

# Commercial Printing
Book is formatted for publishing at large printing houses (or basements).
HOWTO: set up and print with Lulu.


# License
SatNOGS Optical Unofficial Guide

by Jeff Moe

Copyright &copy; 2022, Jeff Moe

Permission is granted to copy, distribute and/or modify this document under
the terms of the Creative Commons Attribution 4.0 International Public License
(CC BY-SA 4.0).

Published by Jeff Moe, Loveland, Colorado, USA.

