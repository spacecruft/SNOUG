%
% SatNOGS_Optical.tex
%
% SatNOGS Optical Unofficial Guide
%
% Copyright (C) 2022, Jeff Moe
%
% This document is licensed under the Creative Commons Attribution 4.0
% International Public License (CC BY-SA 4.0) by Jeff Moe.
%
\section{SatNOGS Optical HOWTO}
\label{sec:optical-howto}

\gls{SatNOGS-Optical} is the nascent distributed network of optical
\glspl{ground-station}.

This chapter gives a top level review what is needed in terms of hardware and
software to build an operating optical \gls{ground-station}.
\index{hardware}\index{software}


\section{Toolchain}
\label{sec:toolchain}

See below for a \gls{SatNOGS-Optical} Process Overview.%
\footnote{\url{https://spacecruft.org/spacecruft/SNOPO}}
See figure \ref{fig:snopo}, page \pageref{fig:snopo}, described below.

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
  \item [Hardware] --- Hardware, such as cameras and \glspl{embedded-system}, is to be selected and set up.
  \item [Software] --- The best currently available software is to be downloaded, installed, and configured.
  \item [Acquire] --- Data samples, typically in the form of \gls{FITS} file photographs, need to be acquired by running a camera outside at night taking pictures of the sky.
  \item [\Gls{plate-solver}] --- Acquired data samples in \gls{FITS} files need to be processed by a \gls{plate-solver}. See section \ref{sec:plate-solver}, page \pageref{sec:plate-solver}.
  \item [Detect \glspl{satellite}] --- Using \glspl{TLE} and the ``solved'' plates, detect \glspl{satellite}. See section \ref{sec:satellite-detection}, page \pageref{sec:satellite-detection}.
  \item [Identify \glspl{satellite}] --- With \glspl{satellite} detected in the previous step, identify what they are. See \ref{sec:overview-identify}, page \pageref{sec:overview-identify}.
\end{description}
\end{mdframed}
\index{hardware}\index{software}\index{acquire}\index{detect}\index{identify}
\index{camera}

\begin{sidewaysfigure}[p!]
	\begin{center}
	\includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{SNOPO.png}
	\caption{\gls{SatNOGS-Optical} Process Overview}
	\label{fig:snopo}
	\index{process}
	\end{center}
\end{sidewaysfigure}

\section{Hardware}
\label{sec:hardware}
\index{hardware}

Discussed in this section are some of the hardware options to be
explored. More explicit instructions of a particular hardware installation
can be see in section \ref{sec:hardware-overview}, page \pageref{sec:hardware-overview}.
Discussed below are camera options, for details on \glspl{embedded-system} and other parts,
also see hardware in section \ref{sec:hardware-overview}, page \pageref{sec:hardware-overview}
\index{camera}

For the purposes here, there are three main categories of hardware. Depending which
category of equipment is selected, it impacts everything else, such as the
software used. Main categories:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
  \item [Motion video cameras] --- Moving images.
  \item [Still camera] --- Still photos.
  \item [Allsky cameras] --- Views of all, or nearly all of the sky.
\end{description}
\end{mdframed}
\index{motion video}\index{still camera}\index{allsky}

Different types of equipment can be used in different categories.
Some can be used in multiple setups, most just in one.
If available, using motion video cameras will work best for
detecting \glspl{satellite} with the developing \gls{SatNOGS} \gls{toolchain}.
Examples of motion video camera sources that could be used:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
  \item [The Imaging Source Cameras based on IMX174] --- Known to work. Recommended.
   High quality cameras, believed to be usable following \gls{DFSG}.
  \item [ZWO ASI based on IMX174] --- Known to work. Not \gls{DFSG} compatible.
   Uses proprietary \gls{SDK}. Currently in prototype development. \index{proprietary}
  \item [\gls{UVC}/\gls{V4L2}] --- ``Any'' video camera that works with the \gls{Linux} kernel.
   Typically, the device will appear similar to \texttt{/dev/video0}. A camera
   that works with the software isn't necessarily sensitive enough to detect
   satellites, however, as most are designed for brighter environments.
  \item [\gls{OpenCV}] --- cameras that work with \gls{OpenCV} can be used, same as \gls{UVC}.
   To work well, they need to be sensitive.
  \item [\gls{Raspberry-Pi}] --- The PiCamera can be used. A good lower cost option.
   Recommended. Many non-\gls{Raspberry-Pi} devices, such as Odroid are also compatible with the Pi
   \gls{MIPI} interface.\index{PiCamera}\index{Odroid}
\end{description}
\end{mdframed}
\index{The Imaging Source}\index{ZWO ASI}

Still cameras can also be used productively. The current \gls{Python} \gls{toolchain}
is in very early development and not completely usable yet.

See the list below for still camera options:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
  \item [\Glspl{telescope}] --- Can definitely take images of \glspl{satellite}.
  Not the best tool at present, as it isn't well integrated into the \gls{SatNOGS-Optical} toolchain.
  The \gls{FoV} is generally too small. The mounts are optimized for
  different types of tracking than satellites. This is changing, and longer
  term could be well-supported. Using \gls{RASA} style \glspl{astrograph}
  is likely the best option.
  \item [\gls{INDI}] --- Typically used for control of \glspl{telescope} and
  associated instrumentation, such as tracking mounts and cameras.
  Cannot be used directly with the current developing \gls{SatNOGS} toolchain.
  It is not seen as the future path forward as it isn't well optimized for
  \gls{SatNOGS-Optical} usage. That said, it is very useful at present for
  running a tracking mount with \gls{KStars} and Ekos, for example,
  in lieu of a better option. Camera software in the \gls{INDI} platform typically
  produce image \gls{FITS} files.
  \item [\gls{gPhoto}] --- The \gls{Linux} kernel recognizes many cameras that can be
  used with \gls{gPhoto} tools and drivers, available in \gls{Debian}.
  This is the recommended option at present for still cameras.
  \gls{DSLR} cameras, such as from major manufacturers Canon and Nikon, are
  used with \gls{gPhoto}.
\end{description}
\end{mdframed}
\index{Ekos}\index{track}\index{mount}\index{Canon}\index{Nikon}

Considering the hardware options above, they need to be matched with
corresponding software. Not all options work (at all), and some cannot be
easily used to perform all steps needed.

There are also broader ``paths'' that need to be considered:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
  \item [\gls{sattools}] --- Deprecated because it is in \gls{C}, and the
    decision by upstream and the \gls{LSF} was made to move forward with applications primarily
    written in \gls{Python}. \Gls{sattools} is the most complete toolkit,
    however, so no matter what path is chosen, some parts of it will likely
    be used for now. It can be used with motion video cameras and
    still cameras. It includes many other software tools related to
    \glspl{satellite}.
  \item [\gls{stvid}] --- This is the best path if a motion video camera
    is available. It is in \gls{Python} and is the tool the
    \gls{SatNOGS-Optical} project is using as the basis for future development.
    It still depends on some \gls{C} tools from \gls{sattools}.
  \item [\texttt{stphot}] --- Written in \gls{Python} this is what the
    \gls{SatNOGS-Optical} project will likely use in the future
    for still cameras.
    It is in very early development, but can acquire data (take photos)
    with \gls{gPhoto}-compatible cameras.
  \item [\texttt{asm}] --- All Sky Monitor for taking pictures of all, or nearly all
    of the sky, such as with a 150 or 180 degree view. The \texttt{asm}
    application is in pre-development, but is in \gls{Python} and could be
    the basis for future \gls{SatNOGS-Optical} development.
    The difficulty with all sky cameras is the \gls{plate-solver} isn't
    written to use images from the ``fish-eye'' view of an all sky camera. 
  \item [Other] --- There are many other satellite and \gls{telescope} software
    packges freely available on the Internet. Perhaps some could be adapted for
    usage.
\end{description}
\end{mdframed}
\index{motion video}
\index{still camera}\index{allsky}\index{stphot}\index{allsky}

