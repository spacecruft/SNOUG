%
% Software.tex
%
% SatNOGS Optical Unofficial Guide
%
% Copyright (C) 2022, Jeff Moe
%
% This document is licensed under the Creative Commons Attribution 4.0
% International Public License (CC BY-SA 4.0) by Jeff Moe.
%

\section{Software Overview}
\label{sec:software-overview}
\index{software}

See \ref{fig:operation}, page \pageref{fig:operation}
for a diagram with an overview of the software process.

\begin{center}
        \begin{figure}[h!]
% XXX diagram is too large for frame
%	\begin{framed}
                \smartdiagramset{border color=none,
                        uniform color list=jebba-light-blue!60 for 4 items,
                        module x sep=3.75,
                        back arrow distance=0.75,
                }
                \smartdiagram[flow diagram:horizontal]{Acquire, Process, Analyze, Upload}
                \caption{\gls{SatNOGS-Optical} Operation Pipeline}
                \label{fig:operation}
                \index{operation}
%	\end{framed}
        \end{figure}
\end{center}
\index{acquire}\index{process}\index{analyze}\index{upload}

\section{Setup Optical Ground Station Software}
\label{sec:software-setup}
\index{software}
HOWTO set up and configure a \gls{SatNOGS-Optical} \gls{ground-station} prototype.
Note: the \gls{SatNOGS} network is not yet ready for optical data.

For a description of installation using \gls{Docker}, see
section \ref{sec:software-docker}, page \pageref{sec:software-docker}.
For a ``manual'' install, see immediately below.

Setup an \gls{embedded-system}, such as a \gls{Raspberry-Pi} or an Odroid N2,
with \gls{Debian} stable (11/Bullseye) or testing (Bookworm).
\index{Odroid}

See each \gls{software-repository} for latest documentation.
\index{repository}

Install dependencies from the \gls{Debian} \gls{software-repository}:

\begin{minted}{sh}
sudo apt update
sudo apt install python3-virtualenv python3-pip python3-dev \
    source-extractor astrometry.net \
    libeigen3-dev giza-dev libx11-dev \
    astrometry-data-tycho2 astrometry-data-tycho2-07 \
    astrometry-data-tycho2-08 astrometry-data-tycho2-09 \
    astrometry-data-tycho2-10-19
\end{minted}
\index{virtualenv}\index{PIP}
\index{Giza}\index{Source Extractor}

Next, install the \texttt{\gls{hough3d-code}} dependency.

\begin{minted}{sh}
git clone https://gitlab.com/pierros/hough3d-code
cd hough3d-code
make all
sudo cp -p hough3dlines /usr/local/bin/hough3dlines
\end{minted}

Install \texttt{\gls{satpredict}} from using either the cbassa or spacecruft
\gls{software-repository}.
\index{repository}

\begin{minted}{sh}
cd ../
git clone https://spacecruft.org/spacecruft/satpredict
# Or
#git clone https://github.com/cbassa/satpredict
cd satpredict/
make
sudo make install
\end{minted}

Now install \texttt{\gls{stvid}}, the main acquisition and processing
application. It is written in \gls{Python}. Either use the spacecruft
\texttt{git} \gls{software-repository} or the cbassa one.
\index{acquire}

\begin{minted}{sh}
cd ../
git clone https://spacecruft.org/spacecruft/stvid
# Or use upstream:
#git clone https://github.com/cbassa/stvid
\end{minted}

Optionally, set up a \gls{Python} virtual environment and use \gls{PIP}:
\index{virtualenv}

\begin{minted}{sh}
cd stvid
virtualenv -p python3 env
source env/bin/activate
pip install --upgrade pip
\end{minted}

Install dependencies. Numpy needs to be installed first, or the
install fails.

\begin{minted}{sh}
pip install --upgrade numpy
pip install --upgrade -r requirements.txt
\end{minted}
\index{numpy}

If the system \texttt{python3-numpy} conflicts, you could try installing
numpy with \gls{PIP} in the virtualenv thusly:

\begin{minted}{sh}
pip install --upgrade --ignore-installed numpy
\end{minted}

\gls{Debian} in bug \texttt{\#941466} changed the name of the
\texttt{sextractor} dependency to \texttt{source-extractor},
so a symlink is needed for now:
\index{Source Extractor}\index{SExtractor}

\begin{minted}{sh}
sudo ln -s /usr/bin/source-extractor /usr/local/bin/sextractor
\end{minted}

\section{Configure Software}
\label{sec:software-configure}
Configure the \gls{embedded-system}.

\begin{minted}{sh}
cd stvid/
cp -p configuration.ini-dist configuration.ini
# Edit
# Note for fields st-username and st-password, *don't* use quotes.
# Set:
vi configuration.ini
\end{minted}
\index{configuration.ini}

Edit various \texttt{configuration.ini} fields:
\begin{minted}{sh}
# For example:
observer_cospar = 9999        # COSPAR number of observer's site
observer_name = Jeff Moe      # Name of observer
observer_lat = 40.568912      # Latitude of location in decimal degrees
observer_lon = -105.225852    # Longitude of location in decimal degrees
observer_height = 1860         # Elevation of location in meters

observations_path = ./data/obs # Path for raw acquisitions
tle_path = ./data/tle          # Path where TLEs are stored (and updated)
results_path = ./data/results   # Path for results of processed obs

st-username = foo
st-password = bar

# Path to source-extractor
sex_config = /usr/share/source-extractor/default.sex
\end{minted}
\index{Source Extractor}\index{COSPAR}\index{TLE}

Store downloads here:

\begin{minted}{sh}
mkdir -p ./data/obs ./data/tle ./data/results
\end{minted}

The \texttt{ST\_DATADIR} variable is still used, from \texttt{sattools}.
Maybe set all these in \texttt{\~/.bashrc} (suit to fit directory setup):
\index{bashrc}

\begin{minted}{sh}
export ST_DATADIR=$HOME/sattools
export ST_TLEDIR=$HOME/TLE
export ST_OBSDIR=$HOME/satobs
export ST_LOGIN="identity=foo@no:pass"
\end{minted}

Set \texttt{astrometry.net} to run in parallel, assuming you have enough \gls{RAM}:
(This doesn't appear to work? Breaks?).

\begin{minted}{sh}
sudo vim /etc/astrometry.cfg
# Uncomment:
inparallel
\end{minted}
\index{astrometry.net}


\section{skymap}
\label{sec:skymap}

Use \gls{skymap} for viewing \gls{satellite} \glspl{orbit} trails
projected on a map of of the sky. \Gls{skymap} is part of \gls{sattools}.
\Gls{skymap} isn't a required part of the \gls{toolchain}, but it is useful
to see what \glspl{satellite} are visible at a particular time and
location.

Source:

* \url{https://github.com/cbassa/sattools}

My fork:

* \url{https://spacecruft.org/spacecruft/sattools}

\begin{sidewaysfigure}[p!]
        \begin{center}
                \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{skymap-giza-21.png}
                \caption{\texttt{\gls{skymap}} example screenshot.}
		\label{fig:skymap-giza-21}
        \end{center}
\end{sidewaysfigure}

\fbox{
        \parbox{\linewidth}{
                \textcolor{red}{NOTICE:} \\
                The main upstream \gls{sattools} package requires non-\gls{libre} \texttt{pgplot} on the host computer which is not \gls{DFSG} compatible.
                \index{proprietary}\index{pgplot}
        }
}

Described below is how to build and install my fork of \gls{sattools} to use
\gls{skymap} instead of upstream. \Gls{upstream} requires non-\gls{libre} software which
also happens to be very difficult for most users to compile. My fork uses the
Giza library which replaces pgplot. It is much easier to build.
\index{fork}\index{Giza}\index{pgplot}

\begin{minted}{sh}
# Install dependencies
sudo apt install giza-dev git make dos2unix source-extractor wcslib-dev \
     libgsl-dev gfortran libpng-dev libx11-dev libjpeg-dev libexif-dev

# Clone repo
git clone https://spacecruft.org/spacecruft/sattools
# Or use upstream (needs more steps)
git clone https://gitub.com/cbassa/sattools

cd sattools/
# Build
make

# Install
sudo make install

# Clean
make clean

# To rebuild
make clean
make

# To uninstall
sudo make uninstall
\end{minted}

See below for \gls{skymap} (fork) usage:


\begin{minted}{sh}
tleupdate

# set config in ~/.bashrc XXX

skymap --help
Usage: skymap [OPTION]
Visualize satellites on a map of the sky.

-t, --time    Date/time (yyyy-mm-ddThh:mm:ss.sss) [default: now]
-c, --catalog    TLE catalog file [default: satnogs.tle]
-i, --id    Satellite ID (NORAD) [default: all]
-R, --ra    R.A. [hh:mm:ss.sss]
-D, --decl    Decl. [+dd:mm:ss.ss]
-A, --azimuth    Azimuth (deg)
-E, --elevation    Elevation (deg)
-w, --width    Screen width (default: 1024). Set height too.
-g, --height    Screen height (default: 768). Set width too.
-n, --nmax    nmax line resolution/speed (default 128)
-S, --all-night    All night
-Q, --no-stars    No stars
-a, --all-objects    Show all objects from catalog (default: LEO)
-h, --help    This help
-s, --site    Site (COSPAR)
-d, --iod    IOD observations
-l, --length    Trail length [default: 60s]
-P, --planar-id    planar search satellite ID
-r, --planar-alt    planar search altitude
-V, --visibility-alt    altitude for visibility contours
-p, --positions-file    File with xyz positions
-L, --longitude    manual site longitude (deg)
-B, --latitude    manual site latitude (deg)
-H, --elevation    manual site elevation (m)
\end{minted}


\section{Tracking Software}
\label{sec:software-tracking}
\index{track}\index{software}\index{mount}
The tracking software used depends upon the tracking mount.

See section \ref{sec:hardware-tracking-ground-station},
page \pageref{sec:hardware-tracking-ground-station}
for discussion of hardware tracking mounts.

For our uses here, there are three broad categories of tracking
software:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
        \item [\Glspl{telescope}] --- Controlling \glspl{telescope} remotely.
        \item [\Glspl{antenna}] --- Controlling \glspl{antenna} remotely with hamlib.
        \item [Cameras] --- Controlling \gls{PTZ} cameras remotely.
\end{description}
\end{mdframed}
\index{hamlib}
\index{camera}


\subsection{Telescope Tracking Software}
Software that can be used with \gls{telescope} tracking mounts:
\index{mount}\index{track}

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
        \item [\gls{INDI}] --- Main client/server used by other applications.
        \item [\gls{KStars}] --- \Glspl{sky-chart}, \gls{INDI} control.
	\item [Ekos] --- Application used within \gls{KStars} for remote control
         of \glspl{telescope} and related hardware via \gls{INDI}.
        \item [Stellarium] --- \Glspl{sky-chart}, has \gls{INDI} plugin.
	\item [Other \gls{INDI}] --- Many more applications work with \gls{INDI}.
	\item [INDIGO] --- Positions itself as a next-generation \gls{INDI} (?).
\end{description}
\end{mdframed}
\index{Ekos}\index{Stellarium}
\index{INDIGO}

Using \gls{INDI} with \gls{KStars} and Ekos on a Sky-Watcher or \gls{Celestron}
\gls{telescope} mount is a known working solution.
\index{Sky-Watcher}\index{Ekos}

\gls{Celestron} \glspl{telescope} are widely used and available for many years.
There is a variety of software written to control the mounts. The below list
covers the drivers (e.g. \gls{INDI}), not the software on top (e.g \gls{KStars}).

\gls{Celestron} Nexstar \gls{telescope} tracking mount drivers:%
\footnote{\url{https://www.indilib.org/telescopes/celestron.html}}%
\footnote{\url{https://github.com/indigo-astronomy/libnexstar}}%
\footnote{\url{https://github.com/indigo-astronomy/indigo/blob/master/indigo_drivers/mount_nexstar/README.md}}
\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
  \item [Celestron INDI] --- \gls{Celestron} \gls{INDI}
  \item [libnexstar INDIGO] --- libnexstar INDIGO.
\end{description}
\end{mdframed}

Sky-Watcher EQ6-R Pro \gls{telescope} tracking mount drivers:%
\footnote{\url{https://www.indilib.org/telescopes/skywatcher.html}}%
\footnote{\url{https://www.indilib.org/telescopes/skywatcher/eqmod.html}}%
\footnote{\url{https://www.indilib.org/telescopes/skywatcher/synscan-telescope.html}}
\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
  \item [EQMod Mount INDI] --- Drives Sky-Watcher protocol.
  \item [Synscan INDI] --- Drives Synscan hand controller.
   \gls{INDI} recommends using EQMod Mount over Synscan.
  \item [libnexstar INDIGO] --- libnexstar INDIGO.
\end{description}
\end{mdframed}

\subsection{INDI}
\gls{INDI} does much, \gls{KStars}, etc.

One option to consider is making \gls{stvid} an \gls{INDI} client.


\subsection{INDIGO}
INDIGO%
\footnote{\url{https://www.indigo-astronomy.org/}}
works with libnexstar, which supports many mounts:%
\footnote{\url{https://github.com/indigo-astronomy/libnexstar}}

\begin{quote}
This library implements the serial commands supported by the Celestron
NexStar hand control (HC) and SkyWatcher/Orion SynScan HC. This applies
to the Celestron: NexStar GPS, NexStar GPS-SA, NexStar iSeries, NexStar
SE Series, NexStar GT, CPC, SLT, Advanced-VX, Advanced-GT, CGE, CGEM etc.
SkyWatcher: EQ5, HEQ5, EQ6 (Pro), AZ-EQ5 GT, AZ-EQ6 GT, EQ8 etc. and the
corresponding Orion mounts.
\end{quote}

\subsection{Antenna Tracking Software}
At present, for the \gls{SatNOGS} network \gls{RF} \glspl{ground-station},
hamlib is typically used for tracking, if directional \glspl{antenna} are
used. Hamlib was originally created for \gls{amateur-radio} equipment, but has
expanded to control many more devices. Hamlib can be used for some
telescope mounts.
\index{hamlib}

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
	\item [hamlib] --- \Gls{antenna} \gls{rotator} control.
\end{description}
\end{mdframed}

\subsection{Hamlib}
I don't know of anyone prototyping a satellite tracking mount with hamlib,
but it should be possible using the ``leap frog'' method, perhaps on the
same mount as existing \gls{SatNOGS} \glspl{antenna}. The mount may not
be steady enough.
\index{hamlib}\index{mount}\index{track}

Hamlib lists support for the following tracking mounts, usually
used with \glspl{telescope}.%
\footnote{Output from version 4.3.1 of \texttt{rotctl --list}}

\begin{minted}{sh}
  1401  Celestron              NexStar                 20110821.0      Untested      ROT_MODEL_NEXSTAR
  1801  Meade                  LX200/Autostar          20200610.0      Stable        ROT_MODEL_MEADE
  1901  iOptron                iOptron                 20191209.0      Alpha         ROT_MODEL_IOPTRON
\end{minted}


\subsection{Camera Tracking Software}

There are applications for using \gls{PTZ} control of cameras, such as used in
``security'' cameras.

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
	\item [motion] --- \gls{PTZ} camera control. XXX check
\end{description}
\end{mdframed}

I don't know of any of these being used in satellite tracking prototypes
at present.


\section{GNSS with \texttt{gpsd}}
\label{sec:software-gnss}
\index{Galileo}

\gls{GNSS} is used for time synchronization and for (somewhat)
accurately determining the location of the observation.

\gls{GNSS} collectively includes the USA \gls{GPS}, Europe's Galileo,
Russia's \gls{GLONASS}, and China's \gls{BDS}, as well as other
regional systems.
\index{Galileo}
\index{USA}\index{Europe}\index{Russia}\index{China}

A basic, widely available \gls{COTS} \gls{USB} \gls{GNSS} device
with a basic (or no!) \gls{antenna} plugged into the \gls{embedded-system}
can get time and location accurate enough for the
purposes here. See various U-Blox devices, for example.
\index{U-Blox}

In \gls{Debian} \gls{GNSS} service with a \gls{USB} device can be provided
by the \texttt{\gls{gpsd}} application.

\begin{minted}{sh}
sudo apt install gpsd gpsd-tools
\end{minted}

Edit the configuration file, \texttt{/etc/default/gpsd} like:

\begin{minted}{sh}
DEVICES="/dev/ttyACM0"
GPSD_OPTIONS="-Gn"
USBAUTO="false"
\end{minted}

This will start a \texttt{\gls{gpsd}} \gls{daemon} listening on all interfaces,
so it can be used by other computers on the network (such as if
acquire and process are on different machines). Using the
device \texttt{/dev/ttyACM0} can be problematic if there are multiple
\texttt{/dev/ttyACM*} devices. Perhaps something like this could be
used in that case:
\index{/dev/ttyACM}

\begin{minted}{sh}
DEVICES="/dev/serial/by-id/usb-u-blox_AG_-_www.u-blox.com_u-blox_GNSS_receiver-if00"
\end{minted}

Even with \texttt{\gls{gpsd}} configuration listening on all \glspl{IP},
don't think \texttt{systemd} won't do what it likes. So
you may have to do:

\begin{minted}{sh}
systemctl edit --full gpsd.socket
\end{minted}
\index{systemd}

And create a \gls{gpsd} configuration like this (I have \gls{IPv6} disabled, in this case):

\begin{minted}{sh}
[Unit]
Description=GPS (Global Positioning System) Daemon Sockets

[Socket]
ListenStream=/run/gpsd.sock
# ListenStream=[::]:2947
ListenStream=0.0.0.0:2947
SocketMode=0600
BindIPv6Only=no

[Install]
WantedBy=sockets.target
\end{minted}
\index{systemd}

(Re)start:

\begin{minted}{sh}
sudo systemctl restart gpsd.service gpsd.socket
\end{minted}
\index{systemctl}

Test all is good:

\begin{minted}{sh}
gpsmon -n
# or
cgps -u m
\end{minted}
\index{gpsmon}\index{cgps}

Enable to start on boot:

\begin{minted}{sh}
sudo systemctl enable gpsd.service gpsd.socket
\end{minted}
\index{systemctl}

See below for sample output from \texttt{cgps}.

\begin{minted}{sh}
┌───────────────────────────────────────────┐┌──────────────────Seen 39/Used 28┐
│ Time:        2022-09-02T00:40:28.000Z (18)││GNSS   PRN  Elev   Azim   SNR Use│
│ Latitude:         40.56896250 N           ││GP  1    1  24.0  237.0  44.0  Y │
│ Longitude:       105.22600420 W           ││GP  3    3  63.0  297.0  50.0  Y │
│ Alt (HAE, MSL):   1848.352,   1869.637 m  ││GP  4    4  33.0  292.0  45.0  Y │
│ Speed:             0.05 km/h              ││GP 16   16  21.0  162.0  36.0  Y │
│ Track (true, var):    85.1,   8.2     deg ││GP 21   21  13.0  218.0  40.0  Y │
│ Climb:             0.00 m/min             ││GP 22   22  40.0   77.0  18.0  Y │
│ Status:         3D DGPS FIX (29 secs)     ││GP 26   26  43.0  140.0  46.0  Y │
│ Long Err  (XDOP, EPX):  0.40, +/-  1.5 m  ││GP 31   31  57.0   49.0  39.0  Y │
│ Lat Err   (YDOP, EPY):  0.36, +/-  1.3 m  ││SB131   44  42.0  198.0  44.0  Y │
│ Alt Err   (VDOP, EPV):  0.90, +/-  0.7 m  ││SB133   46  37.0  214.0  41.0  Y │
│ 2D Err    (HDOP, CEP):  0.51, +/-  0.5 m  ││GL  6   70  48.0   99.0  21.0  Y │
│ 3D Err    (PDOP, SEP):  1.04, +/-  5.1 m  ││GL  7   71  62.0  352.0  51.0  Y │
│ Time Err  (TDOP):       0.57              ││GL  8   72  19.0  314.0  47.0  Y │
│ Geo Err   (GDOP):       1.18              ││GL  9   73  71.0  149.0  45.0  Y │
│ ECEF X, VX:   -1274598.340 m   -0.010 m/s ││GL 10   74  28.0  194.0  29.0  Y │
│ ECEF Y, VY:   -4682900.030 m   -0.010 m/s ││GL 23   87  13.0  305.0  48.0  Y │
│ ECEF Z, VZ:    4127382.070 m    0.020 m/s ││GA  2  302  31.0  168.0  40.0  Y │
│ Speed Err (EPS):       +/-  0.4 km/h      ││GA  3  303  72.0   40.0  39.0  Y │
│ Track Err (EPD):        n/a               ││GA  8  308  40.0  300.0  47.0  Y │
│ Time offset:           -0.005160195 s     ││GA 13  313  22.0  285.0  43.0  Y │
│ Grid Square:            DN70jn26          ││GA 15  315  17.0  232.0  43.0  Y │
└───────────────────────────────────────────┘└More...──────────────────────────┘
\end{minted}
\index{cgps}


\section{NTP}
\label{sec:software-ntp}

With all the above, time still needs to be configured.
Configuring \texttt{\gls{gpsd}} isn't enough for correct system time.
The main system on the Internet used for time synchronization is \gls{NTP}.
In \gls{Debian} there are a few options for \gls{NTP}.
The best is to use a hardware \gls{GNSS} (\gls{GPS}), with \gls{PPS} for improved
accuracy. The easiest is to just use \gls{NTP}.

All systems in the \gls{pipeline} need to have the correct time and
location. It is best if they all pull from the same \gls{NTP}
server, or even better than best if they all run \gls{GNSS}
hardware with \gls{PPS} enabled.

Some quick and dirty time synchronization commands.
The \texttt{ntpd} daemon can have slow startup synchronization
sometimes. If so, perhaps run like this:

\begin{minted}{sh}
# Get the daemon out of the way.
sudo systemctl stop ntp

# Get time from someone, MIT in this case:
sudo ntpdate time.mit.edu

# OR

# Get time from local server, such as router (made up IP):
sudo ntpdate 192.168.1.1

# If above complains, to do an initial sync at boot (force ntpdate):
sudo ntpdate -b 192.168.1.1

# Restart daemon
sudo systemctl start ntp
\end{minted}
\index{ntpd}\index{ntpdate}

Go through each of the systems used in the \gls{toolchain} and make
sure they all have accurate time or processing will be off.


\section{KStars with Tracking Mount}
\label{sec:software-kstars}
\index{software}\index{track}\index{mount}
\index{Ekos}
\gls{KStars} is an application that has been used for years in the astronomy
community. It is well established, well supported, and included in \gls{Debian}.

\gls{KStars} has multiple uses for imaging \glspl{satellite}.
One main use is to control a \gls{telescope} tracking mount, such as the
hardware described in section \ref{sec:hardware-mounts}, page \pageref{sec:hardware-mounts}.
\index{track}\index{mount}

For the purposes here, described below will be using \gls{KStars} with a
Sky-Watcher tracking mount with \gls{INDI} and Ekos.
See figure \ref{fig:video-enclosure-mount-tripod}, page \pageref{fig:video-enclosure-mount-tripod}
for a photo of the setup used with \gls{KStars} below.
\index{Ekos}

The \gls{telescope} mount will be used in sidereal tracking mode,
where the stars will appear as
``points'', and the \glspl{satellite} will appear as trails.
The mount is not used in a satellite tracking mode.
Sidereal is the ``standard'' tracking mode of \glspl{telescope}.
\index{Sky-Watcher}\index{Ekos}

\gls{KStars} is the ``main'' application, but it depends on other key parts.
\gls{INDI} is the protocol that \gls{KStars} uses for \gls{telescope} control.
\gls{INDI} itself is a collection of applications.
While \gls{KStars} has the main \gls{sky-chart} and Ekos is launched within it,
the actual mount control is done with the Ekos application.
While it may sound complex, all of this is set up pretty easily in
\gls{Debian}.
\index{Ekos}\index{mount}

\begin{minted}{sh}
# Quick and dirty from memory, something like:
sudo apt update
sudo apt install kstars indi-bin indi-eqmod indi-gpsd
\end{minted}

\gls{KStars} has a \gls{sky-chart}, as can be see in figure \ref{fig:kstars-skychart},
page \pageref{fig:kstars-skychart}.
When mount control is functioning, a location on the \gls{sky-chart}, such as a star,
 can be clicked on and the mount will \gls{GoTo} that location and optionally track it.
Using this, a \gls{telescope} mount can be used to easily point the camera at a location
and track it to observe \glspl{satellite}. It should also provide a superior \gls{FITS} file
for extracting data than using a static mount with \glspl{star-trail} (XXX made up).
\index{mount}\index{track}
\index{camera}

To use a \gls{telescope} tracking mount for use with \texttt{\gls{stvid}}, the following steps need to be performed in
roughly this order, assuming everything has been configured.
An overview of steps:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{enumerate}
        \item Physically mount all hardware.
	\item Plug in and power everything up.
	\item Confirm all hardware looks ok (e.g. \texttt{lsusb}).
	\item Confirm \gls{GNSS} time and location are ok (e.g. \texttt{cgps -u m}).
         See section \ref{sec:software-gnss}, page \pageref{sec:software-gnss}.
	\item Confirm time is correct on all systems in the \gls{toolchain}
         (e.g. \texttt{date}). 
         See section \ref{sec:software-ntp}, page \pageref{sec:software-ntp},
	for information about \gls{NTP}.
	\item Run camera configuration script (e.g. \texttt{v4l2-ctl} commands).
	\item Start \texttt{indiserver} on the \gls{embedded-system}, using scripts to
         include a camera (such as \texttt{indi\_v4l2\_ccd}.
	\item Start \gls{KStars} on the workstation.
	\item Launch Ekos within \gls{KStars}, under \texttt{Tools}.
	\item In Ekos, select a configuration with the \gls{EQ} Mount for the Sky-Watcher,
         and \texttt{\gls{V4L2}} for the \gls{CCD}, which will work with The Imaging Source
         camera used in this example. Alternatively, the ZWO ASI could be used with a similar configuration.
	\item The Ekos configuration should also be set to use the remote \texttt{indiserver}
	\gls{IP} address of the \gls{embedded-system} \gls{USB} connected to the Sky-Watcher mount.
	\item Hit the start button to start Ekos/\gls{INDI}.
	\item On the screen that pops up, confirm all the tabs are good.
	\item Check the last configuration tab for the camera, it often
         resets the size.
	\item Hit close on the screen with the \gls{INDI} devices.
	\item Click the mount icon, and start tracking.
	\item Perhaps do some focusing... XXX \index{focus}
	\item Click the solver icon.
	\item Click the Sync radio button.
	\item Click Capture and Solve. XXX what is it.
	\item If everything is tracking happily, good.
	\item If not, do all the alignment steps. \index{alignment}
	\item When alignment is good and tracking is accurate, stop Ekos and close it.
	\item Stop the \texttt{indiserver} running on the \gls{embedded-system}.
	\item Start the \texttt{indiserver} on the \gls{embedded-system},
	 but without using a camera (e.g. remove \texttt{indi\_v4l2\_ccd}.
	\item Select the \gls{INDI} configuration with a remote \texttt{indiserver},
         the \gls{EQ} Mount, and the Simulated \gls{CCD}.
	\item Hit start in Ekos to get \gls{INDI} connections going.
	\item Confirm all is ok in hardware tabs, then hit close.
	\item Now in the \gls{KStars} \gls{sky-chart} window there is
         control of the mount without interfering with the camera.
	\item Start \texttt{\gls{stvid}}. See XXX for more info.
	\item When done capturing that part of the sky with \texttt{\gls{stvid}},
         stop \texttt{\gls{stvid}}
	\item Go to the \gls{KStars} \gls{sky-chart} and right-click
         on the new location, and \gls{slew} to it.
	\item Start \texttt{\gls{stvid}} again, pointing at the new location.
	\item Repeat the last few steps each time a new sky location is desired.
\end{enumerate}
\end{mdframed}
\index{v4l2-ctl}\index{mount}\index{track}
\index{lsusb}\index{cgps}\index{indiserver}
\index{Ekos}\index{Sky-Watcher}\index{The Imaging Source}

\begin{sidewaysfigure}[p!]
        \begin{center}
                \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{kstars-skychart.png}
                \caption{\gls{KStars} \gls{sky-chart}, example screenshot.}
                \label{fig:kstars-skychart}
        \end{center}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
        \begin{center}
                \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{kstars-profile-edit-v4l2.png}
                \caption{\gls{KStars} using \gls{V4L2}, example screenshot.}
                \label{fig:kstars-profile-edit-v4l2}
        \end{center}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
        \begin{center}
                \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{kstars-profile-edit-simulator.png}
                \caption{\gls{KStars} using simulated \gls{CCD}, example screenshot.}
                \label{fig:kstars-profile-edit-simulator}
        \end{center}
\end{sidewaysfigure}

If the camera and mount are connected to the \gls{embedded-system} OK, it will
look like below, in this case with The Imaging Source camera and Sky-Watcher
mount:

\begin{minted}{sh}
jebba@odroid-01:~$ lsusb 
Bus 002 Device 003: ID 199e:9004 The Imaging Source Europe GmbH DMK 33UX174
Bus 002 Device 002: ID 05e3:0620 Genesys Logic, Inc. GL3523 Hub
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 003: ID 067b:23d3 Prolific Technology, Inc. USB-Serial Controller 
Bus 001 Device 002: ID 05e3:0610 Genesys Logic, Inc. Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
\end{minted}
\index{The Imaging Source}\index{Sky-Watcher}

Note the \texttt{The Imaging Source Europe GmbH DMK 33UX174} device is the
camera and the
\texttt{Prolific Technology, Inc. USB-Serial Controller} is the
Sky-Watcher \gls{telescope} mount. Other views of the devices:
\index{Sky-Watcher}

\begin{minted}{sh}
jebba@odroid-01:~$ ls -l /dev/bus/usb/002/003 /dev/bus/usb/001/003 /dev/video* /dev/ttyUSB0
crw-rw---- 1 root plugdev 189,   2 Sep  1 20:08 /dev/bus/usb/001/003
crw-rw-rw- 1 root video   189, 130 Sep  1 20:08 /dev/bus/usb/002/003
crw-rw---- 1 root dialout 188,   0 Sep  1 20:08 /dev/ttyUSB0
crw-rw---- 1 root video    81,   0 Sep  1 20:08 /dev/video0
crw-rw---- 1 root video    81,   1 Sep  1 20:08 /dev/video1
crw-rw---- 1 root video    81,   2 Sep  1 20:08 /dev/video2
\end{minted}
\index{/dev/video}\index{/dev/ttyUSB}

Side note: you need to be in the \texttt{plugdev, video, dialout} groups.
Side note: on the Odroid N2, there is another \texttt{/dev/video} device,
so three devices are appearing. The The Imaging Source camera appears
as two \texttt{/dev/video} devices.
\index{Odroid}\index{The Imaging Source}


\section{\texttt{stvid} with Docker}
\label{sec:software-docker}
\index{Ekman, Daniel}

Daniel Ekman has created a \gls{Docker} setup for easier installation
and configuration.

The \gls{Docker} source code repo is available here:

* \url{https://github.com/kng/stvid-docker}

The \gls{Docker} itself is here:

* \url{https://hub.docker.com/repository/docker/knegge/stvid}

