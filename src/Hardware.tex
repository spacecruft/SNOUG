%
% Hardware.tex
%
% SatNOGS Optical Unofficial Guide
%
% Copyright (C) 2022, Jeff Moe
%
% This document is licensed under the Creative Commons Attribution 4.0
% International Public License (CC BY-SA 4.0) by Jeff Moe.
%

\section{Overview of Hardware}
\label{sec:hardware-overview}
\index{hardware}
Hardware considerations for a \gls{SatNOGS-Optical} \gls{ground-station}.

Main hardware components in an \gls{optical-ground-station}:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{itemize}
	\item Lens. \index{lens}
	\item Camera. \index{camera}
	\item \Gls{embedded-system} (computer).
\end{itemize}
\end{mdframed}
\index{lens}\index{camera}

Other components:
\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{itemize}
	\item Ethernet cable. \index{ethernet}
	\item \gls{USB} cable. 
	\item Enclosure. \index{enclosure}
	\item Power supply. \index{power supply}
	\item Tripod. \index{tripod}
	\item Manual or tracking mount. \index{mount}\index{track}
	\item Power source, grid or alternative.
	\item Internet, wifi or ethernet.\index{ethernet}
\end{itemize}
\end{mdframed}
\index{ethernet}\index{enclosure}\index{power supply}
\index{tripod}\index{mount}\index{wifi}

\section{Camera}
\label{sec:hardware-camera}
\index{camera}
% XXX aravis

Cameras being evaluated:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{itemize}
	\item The Imaging Source IMX174 based, monochrome. \index{The Imaging Source}\index{IMX174}
	\item ZWO ASI174MM, monochrome. \index{ZWO ASI}
	\item ZWO ASI1600MM Pro, monochrome. \index{ZWO ASI}
	\item \gls{DSLR} camera. 
	\item PiCamera. \index{PiCamera}
\end{itemize}
\end{mdframed}

\fbox{
	\parbox{\linewidth}{
		\textcolor{red}{NOTICE:} \\
		ZWO/ASI cameras require proprietary non-\gls{libre} software on host computer and is not \gls{DFSG} compatible.
		\index{proprietary}
	}
}

\section{Lenses}
\label{sec:hardware-lenses}
\index{lens}
For lenses, the faster the better.
F1.2 works well.
F1.8 is the maximum recommended.

Lenses being tested:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{itemize}
	\item Kowa 50mm f1.4 C-mount. \index{Kowa}
	\item Canon EF 50mm f1.2 \gls{USM}. \index{Canon}
\end{itemize}
\end{mdframed}

\section{Embedded System}
\label{sec:hardware-computer}
\index{hardware}
\Glspl{embedded-system}, such as \gls{Raspberry-Pi}, that can be used.

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
	\item [Odroid N2] --- Confirmed working. \index{Odroid}
	\item [Odroid M1] --- Testing.
	\item [\gls{Raspberry-Pi} 3] --- ?
	\item [\gls{Raspberry-Pi} 4] --- ?
	\item [Intel \gls{NUC}] --- ? \index{Intel}
\end{description}
\end{mdframed}

\subsection{Embedded Systems Comparison}
Comparing \glspl{embedded-system} for \gls{SatNOGS-Optical}.

\begin{center}
\begin{table}[ht]
	\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
	\begin{center}
	\hspace*{-1.5cm}
	\begin{tabularx}{250pt}{|c|c|c|c|c|}
		\hline
		Make   & Model & Architecture & Max RAM & eMMC\\
		\hline
		Odroid & N2    & ARM64        & 4 GB    & Yes\\
		\hline
		Odroid & M1    & ARM64        & 8 GB    & Yes\\
		\hline
	\end{tabularx}
	\caption{Comparison of embedded systems}
	\label{compare-embed}
	\end{center}
	\end{mdframed}
\end{table}
\end{center}
\index{Odroid}\index{ARM64}\index{eMMC}


\section{Example Optical Ground Station with Tracking}
\label{sec:hardware-tracking-ground-station}
\index{mount}\index{track}
\index{tripod}

This is an example of a tracking \gls{ground-station}.
It is a prototype, so there are lots of mis-matched,
overbuilt/underbuilt parts.
The full setup on tripod, can be seen in
\ref{fig:video-enclosure-mount-tripod}, page \pageref{fig:video-enclosure-mount-tripod}.

A close up of the setup can be seen at
\ref{fig:video-enclosure-mount}, page \pageref{fig:video-enclosure-mount},
showing the Sky-Watcher \gls{telescope} tracking mount,
a Bosch \gls{PoE} camera enclosure,
and through the glass the camera lens.
\index{Sky-Watcher}\index{mount}\index{Bosch}\index{camera}
\index{lens}
In the background is a white \gls{antenna} for \gls{GNSS} (\gls{GPS}) and a solar power setup.
\index{solar power}

\begin{figure}[p!]
	\begin{center}
		\includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{video-enclosure-mount.png}
		\caption{SatNOGS-Optical ground station prototype.}
		\label{fig:video-enclosure-mount}
		\index{mount}\index{camera}
	\end{center}
\end{figure}

The camera lens is protected by the enclosure glass, which is \gls{IP67} (XXX) rated.
See a close up of the front of the enclosure and camera lens in figure
\index{enclosure}\index{lens}\index{camera}
\ref{fig:video-enclosure-front}, page \pageref{fig:video-enclosure-front}.

\begin{figure}[h!]
	\begin{framed}
	\begin{center}
		\includegraphics[keepaspectratio=true,height=0.40\textheight,width=1.00\textwidth,angle=0]{video-enclosure-front.png}
		\caption{Camera enclosure, front side, showing glass and lens.}
		\label{fig:video-enclosure-front}
		\index{enclosure}\index{camera}
	\end{center}
	\end{framed}
\end{figure}

As seen in figure
\ref{fig:video-enclosure-left}, page \pageref{fig:video-enclosure-left},
the left side of the enclosure has a hinge for opening.
The bottom white component is part of the \gls{telescope} mount.
\index{mount}

\begin{figure}[h!]
	\begin{framed}
	\begin{center}
		\includegraphics[keepaspectratio=true,height=0.40\textheight,width=1.00\textwidth,angle=0]{video-enclosure-left.png}
		\caption{Camera enclosure, left side, showing hinge.}
		\label{fig:video-enclosure-left}
		\index{enclosure}\index{camera}
	\end{center}
	\end{framed}
\end{figure}

Figure
\ref{fig:video-enclosure-right}, page \pageref{fig:video-enclosure-right},
shows the right side of the enclosure.
Two mounting bolt access points can be seen on each end.
These are unscrewed with a hex head tool (supplied) to open the enclosure.
\index{enclosure}

\begin{figure}[h!]
	\begin{framed}
	\begin{center}
		\includegraphics[keepaspectratio=true,height=0.40\textheight,width=1.00\textwidth,angle=0]{video-enclosure-right.png}
		\caption{Camera enclosure, right side.}
		\label{fig:video-enclosure-right}
		\index{enclosure}\index{camera}
	\end{center}
	\end{framed}
\end{figure}

The enclosure is opened from the right side, as shown in figure
\index{enclosure}
\ref{fig:video-enclosure-right}, page \pageref{fig:video-enclosure-right}.

\begin{sidewaysfigure}[p!]
	\begin{center}
		\includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{video-enclosure-right-open.png}
		\caption{Camera enclosure, right side, opened.}
		\label{fig:video-enclosure-right-open}
		\index{enclosure}\index{camera}
	\end{center}
\end{sidewaysfigure}

Inside the camera enclosure, as shown in Figure
\index{enclosure}
\ref{fig:video-enclosure-top-open}, page \pageref{fig:video-enclosure-top-open},
is:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{itemize}
	\item The Imaging Source DMX camera with Sony IMX174 \gls{CMOS}. \index{The Imaging Source}\index{IMX174}
	\item Kowa 50mm f1.4 C-mount lens.\index{Kowa}
	\item Odroid N2 running \gls{Debian} \gls{GNU} \gls{Linux} system. \index{Odroid}
	\item Odroid N2 plastic enclosure, large half, hole drilled for ad-hoc mounting.
	\item Blower fan on top, with power cable (came with Bosch enclosure). \index{fan}
	\item Fan, maybe not so useful, with power cable (came with Bosch enclosure).
	\item Camera mounting plate (came with Bosch enclosure). \index{camera}
	\item Camera mounting screws, M6x25 (?).
	\item Ethernet cable, internal, short white (came with Bosch enclosure). \index{ethernet}\index{Bosch}
	\item \gls{PoE} ethernet cable, external, plugged into \gls{PoE} switch for data and power. 
	\item \gls{USB} 3 cable, internal, way too long, needs replacing, from Odroid to camera. XXX flat connector
	\item \gls{USB} 3 cable, external, from Odroid to \gls{telescope} mount. XXX large rectangle connector 
	\item ``Custom'' 12\gls{V} \gls{DC} power cable from Bosch \gls{PoE} to Odroid.
	\item Assorted nuts, bolts, and washers for an ad-hoc standoff height.
\end{itemize}
\end{mdframed}
\index{camera}\index{Kowa}\index{The Imaging Source}\index{Odroid}
\index{fan}\index{power cable}\index{mount plate}\index{Bosch}
\index{ethernet}\index{power cable}

\begin{sidewaysfigure}[p!]
	\begin{center}
		\includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{video-enclosure-top-open.png}
		\caption{Camera enclosure, opened.}
		\label{fig:video-enclosure-top-open}
		\index{enclosure}\index{camera}
	\end{center}
\end{sidewaysfigure}

The top of the enclosure shows weather protection and a sun visor.
\index{enclosure}\index{weather}
See figure \ref{fig:video-enclosure-top}, page \pageref{fig:video-enclosure-top}.

\begin{figure}[h!]
	\begin{framed}
	\begin{center}
		\includegraphics[keepaspectratio=true,height=0.40\textheight,width=1.00\textwidth,angle=0]{video-enclosure-top.png}
		\caption{Camera enclosure, top.}
		\label{fig:video-enclosure-top}
		\index{enclosure}\index{camera}
	\end{center}
	\end{framed}
\end{figure}


\section{Tripods}
\label{sec:hardware-tripod}
\index{hardware}\index{tripod}\index{camera}\index{pier}

The camera setup can be mounted a wide variety of ways,
from just setting the camera somewhere (worst option), to a heavy duty
pier with tracking mount (best option).

At present, most prototype optical ground stations are using static mounts
on tripods.

Tripod and similar options include:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
	\item [No mount] --- Quick and dirty, just hang the camera out somewhere sitting on something.
	\item [Small tripod] --- There are small desk tripods than can be used with lighter
         setups, such as used with a \gls{Raspberry-Pi} PiCamera.\index{tripod}
	\item [Photography Tripod] --- Using a common camera tripod, of which there is a wide
         variety, from light to heavy.\index{tripod}
	\item [\Gls{telescope} Tripod] --- Similar to photography tripods, but typically heavier weight.\index{tripod}
	\item [\Gls{telescope} Portable Pier] --- Similar to a \gls{telescope} tripod, but much heavier, typically
         with a larger center pier post. Still movable, and folds up similar to a photography tripod.\index{pier}
	\item [\Gls{telescope} Pier] --- A wide variety, such as making a roughly 1.5 meter permanent cement post.
\end{description} 
\end{mdframed}
\index{pier}


\section{Mounts}
\label{sec:hardware-mounts}
\index{mount}\index{track}\index{tripod}
For mounts, there are two main types: tracking or static.
By the latter ``static'' mounts, it is meant that the
camera, the tripod, and the mount all stay motionless.
This is what you would get using a camera with a common photography
tripod and a simple mounting plate.

Static mounting options include:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
	\item [Camera plate] --- Commonly used on cameras and readily available for mounting
         cameras to tripods.
	\item [Enclosure plate] --- A flat plate with holes drilled in it to mount the camera
         inside an enclosure.
	\item [``Security'' camera enclosure mount] --- Various mounts exist to mount
         security cameras to posts, walls, etc.
\end{description}
\end{mdframed}
\index{camera}\index{mount}

Tracking mount options to consider include:
\index{mount}\index{track}

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
	\item [Sky-Watcher EQ6-R Pro] --- \Gls{telescope} mount using \gls{INDI}.
	\item [\gls{Celestron}] --- Wide variety of \gls{telescope} mounts using \gls{INDI}.
	\item [iOptron] --- \Gls{telescope} mount with (untested) satellite tracking.
	\item [\gls{INDI} \Gls{telescope} Mounts] --- A wide variety of other \gls{INDI} compatible \gls{telescope} mounts.
	\item [Yaesu G-5500] --- Antenna \gls{rotator}.
	\item [hamlib] --- Other hamlib compatible \glspl{rotator}.
	\item [\gls{Teledyne-FLIR} PTU-5] --- High Performance \gls{PTZ} Unit designed for security cameras (untested, no drivers?).
	\item [Misc \gls{PTZ}] --- Other security camera \gls{PTZ} mounts.
\end{description}
\end{mdframed}
\index{track}\index{mount}\index{Sky-Watcher}\index{Yaesu}\index{hamlib}\index{iOptron}
\index{G-5500}

Tracking mounts aren't widely used, but there is support for them in
\texttt{\gls{stvid}} when acquiring data.
The tracking needs to be set up independently of \texttt{stvid}.
At present, I use \gls{KStars} with Ekos to control a Sky-Watcher EQ6-R Pro
tracking mount.
\index{Ekos}\index{Sky-Watcher}\index{track}

For tracking, there a few different ways to track:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
	\item [Static] --- No tracking, just point at one place in the sky.
         Generates \glspl{star-trail}.
         Generates satellite trails.
	\item [Sidereal tracking] --- Tracks stars.
         Generates stars as points.
         Generates satellite trails.
	\item [Satellite tracking] --- Tracks satellites.
         Generates \glspl{star-trail}.
         Generates satellites as points or potentially larger images
         of the satellite structure.
\end{description}
\end{mdframed}

\subsection{Sidereal Tracking Mounts}
\index{track}\index{mount}
Sidereal tracking (``telescope tracking'') is what \gls{COTS} tracking ``\gls{GoTo}''
\glspl{telescope} from \gls{Celestron} or Sky-Watcher do, for example. They track
the stars, countering the rotation of the Earth to keep the same view
of the sky in the camera's \gls{FoV}. Stars remain as points, even after multi-minute
or multi-hour imaging. This is what is used for ``pretty'' pictures
of stars, nebula, galaxies, etc. 
This is the most common tracking set up, as it has been widely used in
astronomy communities for decades.

Within sidereal tracking mounts, there are yet more options:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
	\item [Fork] --- Fork mount.
	\item [\gls{EQ} fork] --- Fork on \gls{EQ} mount.
	\item [German \gls{EQ}] --- Most common \gls{EQ} mount.
	\item [More] --- Endless variety of available \gls{telescope} mounts.
\end{description}
\end{mdframed}
\index{German \gls{EQ}}\index{fork} %XXX
\index{mount}\index{track}
 
Also related to sidereal tracking is lunar and planetary
tracking, but for our uses all three will be included under sidereal tracking.

To use a sidereal tracking mount for imaging satellites, the camera
must ``leap frog'' the satellite.
At present, my practice is to use a sidereal mount, point at a location with \gls{KStars},
start \gls{stvid}. Then stop \gls{stvid}, move to new location using \gls{KStars},
start \gls{stvid}.
\index{track}\index{camera}

See Software section \ref{sec:software-tracking}, \pageref{sec:software-tracking}
for information on using tracking mounts.

\subsection{Satellite Tracking Mounts}
\index{track}\index{mount}
Of the options between a static mount (no tracking), sidereal tracking,
and satellite tracking, the latter is by far the least common.
In this case, the tracking mount is tracking the satellite itself.
This is much more complex than tracking stars, which it builds upon.
It requires, such as:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{description}
	\item [Time] --- Accurate time, such as from \gls{GNSS} or \gls{NTP}.
	\item [Location] --- Accurate location, also available from \gls{GNSS}.
	\item [\gls{TLE}] --- Need to know the \glspl{satellite}' orbit (accurately!).
        \item [Variable speed tracking] --- \Glspl{satellite} are moving at different
         speeds above, the mount needs to be capable of that.
	\item [Alignment] --- On top of all the gear and software needed,
         the equipment needs to be accurately aligned.
\end{description}
\end{mdframed}
\index{alignment}\index{track}

Most tracking equipment for \glspl{telescope},
cameras, and \glspl{antenna} usually has just a few speeds, such as a \gls{slew} speed
and a sidereal star tracking speed. Sometimes there will be a few steps
of these speeds (e.g. \gls{slew} speeds from 1-9), but not the finely tuned tracking
speeds needed to track a satellite. Oftentimes the \gls{telescope} tracking maximum
speed will be too slow for satellite tracking.
\index{track}

Variable speed tracking (XXX phrase?) is needed for tracking satellites if
the goal is to keep the satellite in the (near) center of the image frame
and leave \glspl{star-trail}. The speed the mount moves needs to be calculated
based upon a recent \gls{orbit} calcuation, such as from a \gls{TLE}.

There are highly skilled amateur astronomers that have captured detailed
pictures of artificial satellites, such as the \gls{ISS} and astronauts doing
space walks, using hand guided \glspl{telescope} with low cost \gls{CCD} imagers.
% XXX ref

There are few options for satellite tracking mounts.
Some new iOptron \gls{telescope} mount \gls{firmware} supports tracking
satellites. This has been largely untested so far, but at present
is likely the best option, if a satellite tracking mount is wanted.
\index{iOptron}\index{track}\index{mount}


\section{Future Designs}
\label{sec:hardware-future}

\index{track}
\index{camera}

There is some discussion of using much larger ``lenses'', such as
a \gls{RASA} ``\gls{telescope}'' (See: \gls{astrograph}).
The primary concern is the lack of \gls{satellite}
tracking mounts, because \gls{telescope} mounts are generally too slow,
and need to ``leap frog'' the \gls{satellite}. \Glspl{rotator} used for \glspl{antenna}
aren't typically stable enough for a camera. 

