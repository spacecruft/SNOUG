%
% Identify.tex
%
% SatNOGS Optical Unofficial Guide
%
% Copyright (C) 2022, Jeff Moe
%
% This document is licensed under the Creative Commons Attribution 4.0
% International Public License (CC BY-SA 4.0) by Jeff Moe.
%

\section{Overview of Satellite Identification}
\label{sec:overview-identify}
\index{identify}\index{locate}

The \gls{LSF} is developing a project called \gls{SIDLOC},
for \gls{satellite} identification and localization. With the
addition of many more \glspl{satellite} in \gls{orbit} there is a growing need
for \gls{SSA}.


\section{\texttt{stvid}'s \texttt{process.py} for Identification}
\label{sec:process-identify}
\index{identify}\index{process}

The best tool for identifying \glspl{satellite} from \gls{FITS} files
is the \texttt{process.py}
script from \texttt{\gls{stvid}}, available here:

* \url{https://github.com/cbassa/stvid}

My fork is here:

* \url{https://spacecruft.org/spacecruft/stvid}


\section{Identifying Satellites with \texttt{satid}}
\index{identify}\index{Giza}

The deprecated \gls{C} application, \texttt{satid} from the \texttt{\gls{sattools}}
package can help identify \glspl{satellite}.
See figure \ref{fig:satid-giza-3}, page \pageref{fig:satid-giza-3}
for output from my Giza port of \texttt{\gls{satid}}.%
\footnote{\url{https://spacecruft.org/spacecruft/sattools/media/branch/spacecruft/img/satid-giza-3.png}}
\index{Giza}

* \url{https://github.com/cbassa/sattools}

My fork is here:

* \url{https://spacecruft.org/spacecruft/sattools}

\begin{figure}[p!]
	\begin{center}
		\includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{satid-giza-3.png}
		\caption{\texttt{satid} example.}
		\label{fig:satid-giza-3}
	\end{center}
\end{figure}


\section{Identification with stvid}
\index{identify}
See section \ref{sec:satellite-detection}, page \pageref{sec:satellite-detection}
for details on identification with \gls{stvid}.
The detection and identification steps are both done in one script.

See figure \ref{fig:stvid-process}, page \pageref{fig:stvid-process}
for an example of from \gls{stvid} automated processing of an input \gls{FITS}
file. The output image is in \gls{PNG} format.
\gls{stvid} will also output other files with results, such as \gls{CSV} files.
\index{process}

\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{stvid/2022-08-23T11:21:34.290.fits.png}
        \caption{Example satellite image processed by \texttt{stvid}.}
        \label{fig:stvid-process}
        \index{process}
\end{sidewaysfigure}

\section{Review of Images}

See the following subsections for example output from \gls{stvid}.


\subsection{Unidentified Satellites}
\index{unidentified}\index{identify}
When \gls{stvid} runs the \texttt{process.py} (or new) script and
it encounters a satellite it cannot identify, it gives it the 
\gls{NORAD-ID} \texttt{90000}. If more unidentified satellites are
detected in the same image, each detection is incremented by one.

See figure \ref{fig:stvid-unidentified}, page \pageref{fig:stvid-unidentified},
 for an example image, with three unidentified satellites.
One is on the left, the other two on the right, next to each other.

See figures \ref{fig:stvid-unidentified-90000}, \ref{fig:stvid-unidentified-90001}, and \ref{fig:stvid-unidentified-90002}, pages \pageref{fig:stvid-unidentified-90000}, \pageref{fig:stvid-unidentified-90001}, and \pageref{fig:stvid-unidentified-90002},
to see an example of \texttt{\gls{stvid}} labelling three identified satellites
with \glspl{NORAD-ID} \texttt{90000}, \texttt{90001}, \texttt{90002}.

\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{id-new-sats/2022-08-23T11:18:12.531.fits.png}
        \caption{stvid processed image, with three unidentified satellites.}
        \label{fig:stvid-unidentified}
        \index{unidentified}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{id-new-sats/2022-08-23T11:18:12.531_90000_unid.png}
        \caption{stvid processed image, with three unidentified satellites, the first labeled with NORAD ID \texttt{90000}.}
        \label{fig:stvid-unidentified-90000}
        \index{unidentified}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{id-new-sats/2022-08-23T11:18:12.531_90001_unid.png}
        \caption{stvid processed image, with three unidentified satellites, the second labeled with NORAD ID \texttt{90001}.}
        \label{fig:stvid-unidentified-90001}
        \index{unidentified}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{id-new-sats/2022-08-23T11:18:12.531_90002_unid.png}
        \caption{stvid processed image, with three unidentified satellites, the third labeled with NORAD ID \texttt{90002}.}
        \label{fig:stvid-unidentified-90002}
        \index{unidentified}
\end{sidewaysfigure}


\subsection{Airplanes}
\index{airplane}\index{noise}
Airplanes are noise in the data.

Airplanes will appear not infrequently in images.
See figure \ref{fig:stvid-airplane1}, page \pageref{fig:stvid-airplane1}
for an example with an airplane in the left side, forming two parallel
trails from the aircrafts' lights.

In figure \ref{fig:stvid-airplane2}, page \pageref{fig:stvid-airplane2},
is shown an example image with an airplane with a very distinct blinking light.
 
In figure \ref{fig:stvid-airplane3}, page \pageref{fig:stvid-airplane3},
airplane solid parallel trails can be seen with blinking lights.
 
\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{airplane/2022-08-23T04:17:27.161.fits.png}
        \caption{Airplane, bold left side, in stvid processed image.}
        \label{fig:stvid-airplane1}
        \index{airplane}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{airplane/2022-08-24T03:05:30.370.fits.png}
        \caption{Airplane, blinking, in stvid processed image.}
        \label{fig:stvid-airplane2}
        \index{airplane}
\end{sidewaysfigure}

\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{airplane/2022-08-24T09:18:02.277.fits.png}
        \caption{Airplane, two solid lights plus blinking, in stvid processed image.}
        \label{fig:stvid-airplane3}
        \index{airplane}
\end{sidewaysfigure}


\subsection{Configuration and Setup Errors in Images}
\index{process}\index{identify}

See figure \ref{fig:stvid-misconfigured}, page \pageref{fig:stvid-misconfigured},
for an example of an image processed by a mis-configured system.
As can be seen, the plotted lines are offset from the actual images of satellites.
This, and similar issues, can be addressed by checking:

\begin{mdframed}[backgroundcolor=blue!10,linecolor=blue!30]
\begin{itemize}
  \item Recent \glspl{TLE} on both the processing workstation and the
   \gls{embedded-system}.
  \item Correct, \gls{NTP} (or better) synchronized time on on the processing
   workstation and the \gls{embedded-system}.
  \item Correct latitude, longitude, and altitude are set in configuration
   files, typically based on \gls{GNSS} readings.
   \index{latitude}\index{longitude}\index{altitude}
\end{itemize}
\end{mdframed}


\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{stvid/2022-08-25T03\:40\:03.853.fits.png}
        \caption{\texttt{stvid} processed image, showing mis-configured/setup system.}
        \label{fig:stvid-misconfigured}
        \index{airplane}
\end{sidewaysfigure}

\subsection{Image Processed with Bad TLE}
In figure \ref{fig:stvid-process-bad-tle}, page \pageref{fig:stvid-process-bad-tle},
can be seen a \gls{FITS} file that has been processed by stvid, but with \glspl{TLE}
that are around two days old. As can be seen, the satellites don't quite line up
where they should.


\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{stvid/2022-09-04T03:40:07.923.fits.png}
        \caption{\texttt{stvid} processed image, alignment off due to use of old TLE.}
        \label{fig:stvid-process-bad-tle}
\end{sidewaysfigure}

\subsection{Image Acquired During Slew}
\index{acquire}

See figured \ref{fig:stvid-process-bad-slew}, page \pageref{fig:stvid-process-bad-slew},
for an example image that is bad due to \gls{slew}[ing] during acquisition.

\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{stvid/2022-09-04T04\:56\:37.072.fits.png}
        \caption{\texttt{stvid} processed image, trails due to slewing during acquire.}
        \label{fig:stvid-process-bad-slew}
\end{sidewaysfigure}

\subsection{Light Outside FoV Artifact}
\index{acquire}

See figured \ref{fig:stvid-light-fov}, page \pageref{fig:stvid-light-fov},
for an example of an artifact, visible crossing the left and center,
generated by light outside the \gls{FoV}, in
this case the moon!%
\footnote{Pierros in SatNOGS Optical Matrix}
This is also an example image that detects classified satellites.
This is how the main upstream \texttt{\gls{stvid}} works.

\begin{sidewaysfigure}[p!]
        \includegraphics[keepaspectratio=true,height=1.00\textheight,width=1.00\textwidth,angle=0]{stvid/stvid-light-fov.png}
        \caption{\texttt{stvid} processed image, with classified satellites and light artifact.}
        \label{fig:stvid-light-fov}
\end{sidewaysfigure}

