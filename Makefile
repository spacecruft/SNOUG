# Makefile

all:
	$(MAKE) clean
	$(MAKE) -C src
	mv src/*.pdf .
# ps2pdf breaks ToC and other internal links
#	ps2pdf SNOUG.pdf SNOUG-web.pdf

cover:
	$(MAKE) -C src cover
	mv src/Cover.pdf .

clean:
	rm -f *.pdf
	$(MAKE) clean -C src

